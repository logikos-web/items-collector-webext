# Installation Guide

After you have running the the coraweb-lb server:

Tested on:

node: `6.11`

## Install dependencies

`npm install`

## Load extension

On **Chrome**:

Go to `chrome://extensions`, load unpacked extension...

Select the extension's root folder.

On **Firefox**:

Go to `about:debugging#addons` and click on load temporary Add-on. Select the `manifest.json` file.