# README #

# About the project #

Cross-Origin Reccomendations for Augmenting the Web (from now on, CORA) is a tool that allow the user to collect and structure existing content over the Web to be used as recommendations.

The «Items Extractor» is a browser extension allowing users to extract and submit to a repo all the instances of a pre-defined concept found in the current Web page. Such concept is specified in a template, created with the «Templates Editor» (another extension), which after creation is available in a repo. This tool downloads all the matching templates for the current URL and uses them for identifying instances of a concept in the Web page. The user is presented with the identified elements, highlighted in the DOM, and he may choose which of them (and which properties) should be used for populating a repo used as the source of recommendations.


## This repo contains: ##
The source code of the system, implemented as a Web Extension. To understand better its anatomy, please consider reading:
https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Anatomy_of_a_WebExtension

Please, read carefully such article, since rules are not always the same. E.g. DOM elements can not be fully manipulated from any script, for instance, the event listeners can not be added from popups but the popup can call a contentscript to do so.

## System Requirements ##

### Firefox 57 onwards ###
Starting from Firefox 57, WebExtensions will be the only supported extension type. Please, make you sure you are using at least version 57. 

### NPM ###

We have a bower file for downloading the dependencies that the project needs. This way, we are not distributing the code of the dependencies. We need those files to be part of the extension because some need to be injected from the extension itself and not through a CDN.
sudo apt-get install npm, nodejs
sudo chown -R $USER:$GROUP ~/.npm
sudo chown -R $USER:$GROUP ~/.config
...

## Loading and debugging the extension in Firefox ##

Download dependencies. Open a console at the sidebar/ folder (in the root dir) and execute:
npm install

Open "about:debugging" in Firefox, click "Load Temporary Add-on" and select any file in your add-on's directory. It will be installed until you restart Firefox. 

After installing the extension, you can see a «debug» button next to the extension, as well as an «update». You ca use it but sometimes it is not logging things. The best way is still using the browser's toolbox (CTRL+SHIFT+ALT+i).

If you are trying to debug a "popup script", you should prevent the popups to be closed. To do so, click the "4 sqaures icon" in the right-top area of the browser's toolbox. 

If the button for debugging is not enabled, please activate the "Browser Toolbox"
https://developer.mozilla.org/en-US/docs/Tools/Browser_Toolbox

If you need to access the *options page for configuration*, navigate to "about:addons" after installing the extension. There you can configure the template's repository.

### About us ###

Laboratorio de Investigación y Formación en Informática Avanzada
http://www.lifia.info.unlp.edu.ar/lifia/es/investigacion-capacidades
