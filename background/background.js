class CoraHandler {

  static _showDisabledIcon() {
    browser.browserAction.setIcon({ path: "./resources/icon_disabled_19.png" });
  }

  static _showDefaultIcon() {
    browser.browserAction.setIcon({ path: "./resources/icon_19.png" });
  }

  static _showAskIcon() {
    browser.browserAction.setIcon({ path: "./resources/icon_found_19.png" });
  }

  static _hideBadge() {
    browser.browserAction.setBadgeText({ text: '' });
  }

  static _showBadge(text) {
    browser.browserAction.setBadgeText({ text });
  }

  static templatesNotFound(request, sender, sendResponse) {
    this._showDisabledIcon();
    this._hideBadge();
  }

  static templatesFound(request, sender, sendResponse) {
    this._showAskIcon();
    this._hideBadge();
  }

  static itemsFound(request, sender, sendResponse) {
    this._showDefaultIcon();
    this.setCountBadge(request.total);
  }

  static setCountBadge(num) {
    const total = (num < 100) ? `${num}` : '99+';
    this._showDefaultIcon();
    this._showBadge(total);
  }
}

browser.runtime.onMessage.addListener((request, sender, sendResponse) => {
  CoraHandler[`${request.method}`](request, sender, sendResponse);
});