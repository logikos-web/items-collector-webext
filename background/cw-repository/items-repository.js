class ItemsRepository {
  pushItems(uri, itemType, params) {} 
};
window.ItemsRepository = ItemsRepository;

class RemoteItemsRepository extends ItemsRepository {

  pushItems(uri, itemType, params) {

    //TODO: this method should return a Promise 
    return new Promise((resolve, reject) => {

      browser.storage.local.get("config").then(function(result) {
        var dashboardAccessCode = result.config["dashboard-access-code"];
        var endpoint = result.config["items-repo-uri"];
        const payload = Object.assign({}, { //TODO: this is the same step as in the local strategy 
          owner: 'no_reply@lifia.info.unlp.edu.ar',
          type: itemType,
          url: uri,
          groups: [dashboardAccessCode],
        }, params);

        const req = new XMLHttpRequest();
        req.open('PATCH', endpoint, false);
        req.setRequestHeader("Content-type", "application/json");
        const postItemRequest = JSON.stringify(payload);
        req.send(postItemRequest);

        resolve(req);
      });
    });
  }
};
window.RemoteItemsRepository = RemoteItemsRepository;

class LocalItemsRepository extends ItemsRepository {
  
  pushItems(uri, itemType, params) {

    var payload = Object.assign({}, {
      type: itemType,
      url: uri
    }, params);

    browser.storage.local.get("config").then(function(result) {
      console.log(result.config["items-storage-local-plugin-id"]);
      browser.runtime
        .sendMessage(result.config["items-storage-local-plugin-id"], {
            methodName: "store", arguments: payload 
        })
        .catch((error) => {
            console.log("Could not talk to the storage", error);
            return Promise.reject("Could not talk to the storage");
        });
        return Promise.resolve(payload);
    });
  }
}
window.LocalItemsRepository = LocalItemsRepository;