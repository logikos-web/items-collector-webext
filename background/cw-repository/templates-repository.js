class TemplatesRepository {

  static getTemplatesFor(uri) {

  	return new Promise((resolve, reject) => {

        browser.storage.local.get("config").then(function(result) {

			var templatesUri = result.config["templates-repo-uri"];
			uri = encodeURIComponent(uri);
		    var endpoint = `${templatesUri}/matching?url=${uri}`;
		    var req = new XMLHttpRequest();

			req.open('GET', endpoint, false); 
			req.send(null);
			
			var results = (req.responseText)? JSON.parse(req.responseText): []; //axios.get(endpoint).then(res => res.data.response);
			resolve(results);
		});
    });
  }
}