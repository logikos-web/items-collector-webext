class ItemsCollector {

  constructor(uri, options) {

    this.uri = uri;
    this.owner = options.owner;
    this.groups = options.groups;
    this.extractor = new ItemsExtractor(uri);
    this.enabledStatusByTab = {};
  }

  isHighlightingEnabled(currentTab){

    return this.enabledStatusByTab[currentTab.id] || false;
  }

  enableHighlightingStatus(status, tabId){
    var me = this;
    browser.tabs.query({currentWindow: true, active: true}).then(function(tabs){
      me.enabledStatusByTab[tabs[0].id] = true;
    });
  }

  disableHighlightingStatus(){
    var me = this;
    browser.tabs.query({currentWindow: true, active: true}).then(function(tabs){
      me.enabledStatusByTab[tabs[0].id] = false;
    });
  }

  enableHighlighting(currentTab){

    this.getTemplates().then(t => {
      this.highlightDomElements(t.templates, currentTab);
      this.enableHighlightingStatus();
    });      
  }

  disableHighlighting(currentTab){
    browser.tabs.sendMessage(currentTab.id, {
      call: "disableHighlighting"
    });
    this.disableHighlightingStatus();
  }

  pushItems(args){

    return new Promise((resolve, reject) => {

      browser.storage.local.get("config").then(function setCurrentChoice(result) {
        console.log(result.config["items-storage-startegy"]);
        console.log(window);
        resolve((new window[result.config["items-storage-startegy"]]()).pushItems(args.uri, args.itemType, args.params));
      });
    });

    //alert('Enviado al server con éxito!');
    /*this.pushSelectedItems(args)
                .then((res) => alert('Enviado al server con éxito!'))
                .catch((err) => alert(err));*/
  }

  highlightDomElements(templates, currentTab){

    browser.tabs.sendMessage(currentTab.id, {
      call: "highlightDomElements", 
      args: { 
        "templates": templates
      }
    });
  }

  /**
   * Retrieves templates of the current URI
   *
   * @return {object} {found: <bool>, items: <array>}
   */
  getTemplates() {

    var me = this;

    return new Promise((resolve, reject) => {

        me.extractor.canExtract().then(function(success){

          const res = { found: success, templates: [], count: 0 };
          if (success) {
            me.extractor.getTemplates().then(function(tmpts){

              console.log("tmpts", tmpts);
              res.templates = tmpts;
              res.count = tmpts.length;

              resolve(res);
            });

          } else resolve(res);
        });
    });
  }

  async getItems(templates) {

    var extractedItems = await this.extractor.getItems(templates);
    return extractedItems;
  }

  

  // ------------------------------------------------
  // Private methods
  // ------------------------------------------------

}