//import TemplatesRepo from '../cw-repository/templates-repository';
class ItemsExtractor {

  constructor(uri) {
    this.uri = uri;
    this.templateRepo = TemplatesRepository;
  }

  canExtract() {
    var me = this;
    return new Promise((resolve, reject) => {
        me.getTemplates().then(function(templates){
          resolve(templates.length > 0);
        });
    });
  }

  getTemplates() {
    var me = this;
      return new Promise((resolve, reject) => {
        // resolve(uneValeur); // si la promesse est tenue
        // reject("raison d'echec"); // si elle est rompue
        browser.tabs.query({currentWindow: true, active: true}).then(function(tabs){
          me.templateRepo.getTemplatesFor(tabs[0].url).then(function(tmplates){
            resolve(tmplates);
          }); 
        }, function(){console.log("error")});
      });
  }

  /**
   * Get items by templates
   * 
   * @param {array} templates
   * @return {array} of propertySelectors objects 
   */
  async getItems(templates) {
    try {
      const res = await this.getTemplates();
      // Get the properties for the templates passed by arg
      // and return just the propertySelectors property
      /*var elems = res
        .filter(template => templates.find(t => t.id == template.id))
        .map(template => template.propertySelectors);*/

      return elems;
    } catch (err) {
      console.log('Error on getItems()', err);
      return [];
    }
  }
}