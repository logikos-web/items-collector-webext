Storage.getAll("akey").then(function init(storedData) {

	//Este ItemsCollector en realidad es mucho más. Es el facade del lado background de la extensión
	const collector = new ItemsCollector(window.location.href, storedData); 

	// Listener for the toolbar button
	browser.browserAction.onClicked.addListener(function() {

		browser.tabs.query({currentWindow: true, active: true}).then(function(tabs){

			if(collector.isHighlightingEnabled(tabs[0]))
				collector.disableHighlighting(tabs[0]);
			else collector.enableHighlighting(tabs[0]);
		});
	});

	// Register event listeners
	browser.runtime.onMessage.addListener((request, sender, senderResponse) => {
		return collector[request.type](request.args);
	});
});