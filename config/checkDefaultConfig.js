browser.storage.local.get("config").then(function checkDefaultValues(result) {
  if (result.config) return;
  browser.storage.local.set({
    config: {
      "dashboard-access-code": "",
      "templates-repo-uri": "http://localhost:8080/logikos-api/templates",
      "items-storage-startegy": "RemoteItemsRepository",
      "items-storage-local-plugin-id": "",
      "items-repo-uri": "http://localhost:8080/logikos-api/items"
    }
  });
});
