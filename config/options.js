document.addEventListener("DOMContentLoaded", function restoreOptions() {
  
  document.querySelector("#templates-repo-uri-label").innerHTML = browser.i18n.getMessage("templates_repo_uri");
  document.querySelector("#save").innerHTML = browser.i18n.getMessage("save");
  
  browser.storage.local.get("config").then(function setCurrentChoice(result) {
    document.querySelector("#dashboard-access-code").value = result.config["dashboard-access-code"];
    document.querySelector("#templates-repo-uri").value = result.config["templates-repo-uri"];
    document.querySelector("#items-storage-startegy").value = result.config["items-storage-startegy"];
    document.querySelector("#items-storage-local-plugin-id").value = result.config["items-storage-local-plugin-id"];
    document.querySelector("#items-repo-uri").value = result.config["items-repo-uri"];
    updateOptions();
  });

  document.querySelector("form").addEventListener("submit", function saveOptions(e) {
    e.preventDefault();
    browser.storage.local.set({
      "config": {
        "dashboard-access-code": document.querySelector("#dashboard-access-code").value,
        "templates-repo-uri": document.querySelector("#templates-repo-uri").value,
        "items-storage-startegy": document.querySelector("#items-storage-startegy").value,
        "items-storage-local-plugin-id": document.querySelector("#items-storage-local-plugin-id").value,
        "items-repo-uri": document.querySelector("#items-repo-uri").value
      }
    });
  });

  document.querySelector("#items-storage-startegy").onchange = function(){
    updateOptions();
  };

  function updateOptions(){
    if ( document.querySelector("#items-storage-startegy").value == "LocalItemsRepository" ){
      document.querySelector("#items-storage-local-plugin-id").parentNode.style.display = "block";
      document.querySelector("#items-repo-uri").parentNode.style.display = "none";
    }else{
      document.querySelector("#items-repo-uri").parentNode.style.display = "block";
      document.querySelector("#items-storage-local-plugin-id").parentNode.style.display = "none";
    }
  }

});

