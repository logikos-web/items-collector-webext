function PageSelector(){

    this.itemType="";
	this.items = {};
	var me = this;

	this.clean = function() {

	    $('.cora-overlay').remove();
	    $('.cora-can-extract').remove();
	    this.items = {};
	    browser.runtime.sendMessage({ "type": "disableHighlightingStatus" });
	}

	this.disableHighlighting = function() {

	    this.clean();
	}

	this.pushSelectedItems = function() {

	    if (!Object.keys(this.items).length) {
	      throw Error("There are no items selected to push.");
	    }
	    const params = {
	      properties: this.items
	    }
	    if (this.owner) {
	      params.owner = this.owner;
	    }
	    if (this.groups) {
	      params.groups = this.groups.split(',');
	    }

	    return browser.runtime.sendMessage({ 
	    	"type": "pushItems", 
	    	"args": {"uri": window.location.href, "params": params, "itemType": this.itemType} 
	    });
	}

	this._getOriginalKey = function(key) {
		return key.replace(/^cora-item-/, '');
	}

	this._addItem = function({ id, content }) {
		const key = this._getOriginalKey(id);
		this.items[key] = content;
	}

	this._removeItem = function(id) {
		const key = this._getOriginalKey(id);
		delete this.items[key];
	}

	this.highlightDomElements = function(args){

		var instances = this.getInstances(args.templates);

		this._createOverlay();
		this._bindEvents();
		this.highlightInstances(instances);
	}
	this.getInstances = function(templates){

		var instances = [];
        // TODO: if we handle multiple templates, we should consider their itemTypes independently
        this.itemType= templates[0].itemType;

		for (var i = templates.length - 1; i >= 0; i--) {

			var instance = {};
			Object.keys(templates[i].propertySelectors).forEach(function(key) {
				var xpath = templates[i].propertySelectors[key];
				var elem = new XPathInterpreter().getSingleElementByXpath(xpath, document);
				if (elem != null) {
					instance[key] = (elem);
				}

			});
			instances.push(instance);
		}
		return instances;
	}
	this.highlightInstances = function(instances){  

		var me = this;
		for (var i = instances.length - 1; i >= 0; i--) {
			Object.keys(instances[i]).forEach(function(key) {		
				me.highlightInstance(instances[i][key], key);
			});
		}	    
	}
	this._createOverlay = function() {
    	$('body').append($(`
	      <div id="cora-overlay" class="cora-overlay">
	        <div class="cora-actions">
	          <button id="cora-button-select-all">Check all</button>
	          <button id="cora-button-deselect-all">Uncheck all</button>
	          <button id="cora-button-cancel">Collect item(s)</button>
	          <button id="cora-button-close">x</button>
	        </div>
	      </div>`
	    ));
	}
	this._bindEvents = function() {
		
	    document.querySelector('#cora-button-cancel').onclick = function(){
	    	me.pushSelectedItems().then(function(){
		    	me.clean();
		    });
		}

	    document.querySelector('#cora-button-close').onclick = function(){
	    	me.clean();
		}

	    $('body').on('click', '.cora-can-extract input[type="checkbox"]', (e) => {
	      const parent = $(e.target).closest('.cora-can-extract');
	      const id = $(parent).prop('id');
	      const content = $(parent).find('.content').text().trim();
	      (e.target.checked) ? this._addItem({ id, content }) : this._removeItem(id);
	    });

	    document.querySelector('#cora-button-select-all').onclick = function(){
	      $('.cora-can-extract')
	        .find('input[type=checkbox]:not(:checked)')
	        .trigger('click');
	    };

	    document.querySelector('#cora-button-deselect-all').onclick = function(){
	      $('.cora-can-extract')
	        .find('input[type=checkbox]:checked')
	        .trigger('click');
	    };
	}
	this.highlightInstance = function(singleInstance, key){

		try{
			singleInstance = $(singleInstance);
			// singleInstance.css({'border':'5px solid yellow','background-color':'yellow' });

			const position = singleInstance.offset();
		    const width = singleInstance.width() + 50;
		    const height = singleInstance.height();
		    const itemText = singleInstance.html();

		    const $input = $(`
		      <div class="cora-can-extract" id="cora-item-${key}">
		        <div class="content">${itemText}</div>
		        <div class="check">
		          <input type="checkbox" />
		        </div>
		      </div>`)
		      .css(position)
		      .css({ width });

		    $('body').append($input);

		}catch(err){ console.log("trying to hihglight", singleInstance);
		console.log("got error: ", err); }
	}
};

var pageManager = new PageSelector();
browser.runtime.onMessage.addListener(function callPageSideActions(request, sender, sendResponse) {

	if(pageManager[request.call]){
		pageManager[request.call](request.args);
	}
});