function XPathInterpreter() {
    if ( arguments.callee.instance )    //Singleton pattern
        return arguments.callee.instance;
    arguments.callee.instance = this;

    this.getSingleElementByXpath = function(xpath, node) {

        try{
            var doc = (node && node.ownerDocument)? node.ownerDocument : node;
            var results = doc.evaluate(xpath, node, null, XPathResult.ANY_TYPE, null); 
            return results.iterateNext(); 
        }catch(err){ console.log(err); }
    };
};